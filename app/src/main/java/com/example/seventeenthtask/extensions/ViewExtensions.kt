package com.example.seventeenthtask.extensions

import android.app.Dialog
import android.view.Window
import android.widget.Button

fun Dialog.showDialogWindow(btn1 : Button, btn2 : Button){

    window!!.requestFeature(Window.FEATURE_NO_TITLE)

    btn1.setOnClickListener {
        cancel()
    }
    btn2.setOnClickListener {
        cancel()
    }

    show()
}