package com.example.seventeenthtask

import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import com.example.seventeenthtask.databinding.ActivityMainBinding
import com.example.seventeenthtask.databinding.DialogLayoutBinding
import com.example.seventeenthtask.extensions.showDialogWindow

class MainActivity : AppCompatActivity() {

    lateinit var binding : ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        dialogWindow()
    }

    private fun dialogWindow(){
        val dialog = Dialog(this)

        val binding = DialogLayoutBinding.inflate(layoutInflater)

        dialog.showDialogWindow(binding.agreeBtn, binding.disagreeBtn)

        dialog.setContentView(binding.root)


        //Everything below taken into Extension Function
        /**binding.agreeBtn.setOnClickListener {
            dialog.cancel()
        }
        binding.disagreeBtn.setOnClickListener {
            dialog.cancel()
        }

        dialog.show()*/
    }

}